#!/bin/sh

if [ ! -x $0 ]; then
        chmod +x $0
fi
GO_TARGET="$(dirname ${D})/go"

do_configure() {
        echo "configuring"
}

do_build() {
        echo "building" 1>&2
        echo "GOARM: '${GOARM}'"
        cd src && bash ./all.bash
}

do_install() {
        echo "installing to ${GO_TARGET}"
        install -d ${GO_TARGET}
        install -m 0755 ${WORKDIR}/go ${D}
}

if [ "${TARGET_ARCH}" = "arm" ]; then
        export GOARCH=arm
        export GOOS=linux
        export GOARM=${TARGET#armv%?}
fi

env 1>&2

if [ -z "$1" ]; then
        do_build
elif [ "$1" = "configure" ]; then
        do_configure
elif [ "$1" = "build" ]; then
        do_build
elif [ "$1" = "install" ]; then
        do_install
fi


