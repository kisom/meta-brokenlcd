DESCRIPTION = "The Go programming language."
HOMEPAGE = "http://golang.org"
LICENSE = "BSD"
DEPENDS = "bash"
GOARM = "${${ARCH#arm}%?}"
SRC_URI = "\
	http://go.googlecode.com/files/go1.0.3.src.tar.gz \
	file://make.py \
           "
# For tarball packages (as opposed to git / svn which include the commit in the URI)
SRC_URI[md5sum] = "31acddba58b4592242a3c3c16165866b"
SRC_URI[sha256sum] = "7fba3533d172f13629d3d8a79e57c620632b0bd075abe11d7698b338be0ae3df"
PV = "1.0.3"
S = "${WORKDIR}/go"
LIC_FILES_CHKSUM = "file://${S}/LICENSE;md5=591778525c869cdde0ab5a1bf283cd81"
do_configure () {
        cp ${WORKDIR}/make.py ${S}/make.py
        #cp ${WORKDIR}/make.sh ${S}/make.sh
        #TARGET_ARCH=${TARGET_ARCH} TARGET=${TARGET} D=${D} sh ./make.sh config
}
do_compile () {
        echo "kicking off build"
	GOROOT_FINAL=${D}${prefix}/go TARGET_ARCH=${TARGET_ARCH} TARGET=${TARGET} D=${D} ./make.py build
}
do_install () {
	echo "installing to ${D}${prefix}/go"
	install -d ${D}${prefix}
	cp -r ${S} ${D}${prefix}/go
	chmod 0755 ${D}${prefix}/go
	chown -R root:root ${D}${prefix}/go
}
