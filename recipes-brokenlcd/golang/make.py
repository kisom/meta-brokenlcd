#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Build script for the brokenlcd golang port to the Ångström linux
distribution.
"""

import os
import re
import sys

MARCH = r'^.+ -march=armv(\d+).+$'


def setenv(key, value):
    """
    Mimic setenv() without using os.putenv() (which doesn't update os.environ).
    """
    os.environ[key] = value


def setup_environment(stage):
    """
    Load the config from the environment.
    """
    ccld = os.getenv('CC')
    arch = os.getenv('TARGET_ARCH')

    if arch == 'arm':
        armv = re.sub(MARCH, r'\1', ccld)
        config_arm(armv, stage)
    else:
        if not arch in ['i386', 'x86_64', 'arm']:
            print 'architecture not supported!'
            exit(1)


def config_arm(armv, stage):
    """
    Configure build for the ARM platform.
    """
    setenv('GOARM', armv)
    setenv('GOARCH', 'arm')
    #setenv('USE_GO_TOOL', 'false')
    if stage == 1:
        return
    setenv('GOOS', 'linux')
    setenv('GOHOSTOS', 'linux')
    setenv('GOHOSTARCH', 'arm')


def configure():
<<<<<<< HEAD
    """
    Hook to allow running configuration tasks.
    """
    pass

def build():
    """
    Run the build commands.
    """
    setup_environment(1)
    topdir = os.path.basename(os.getcwd())
    if topdir != 'go' and topdir != 'src':
        if 'go' in os.listdir('.'):
            os.chdir('go/src')
        else:
            print 'Lost in the build directory!'
            print 'Please run from the go toplevel.'
            exit(1)
    elif topdir == 'go':
        os.chdir('src')

    os.system('./make.bash')
    setup_environment(2)
    os.system('./make.bash')


def install():
    """
    Hook to allow running install tasks.
    """
    pass


def main():
    """
    Carry out tasks based on command line.
    """

    cmd = None
    if len(sys.argv) > 1:
        cmd = sys.argv[1]
    else:
        cmd = 'build'

    if not cmd in ['build', ]:
        print 'unrecognised command: %s' % (cmd, )
        exit(1)

    if cmd == 'build':
        build()

if __name__ == '__main__':
    main()
