DESCRIPTION = "Terminal multiplexer."
HOMEPAGE = "http://libevent.org"
LICENSE = "BSD"
DEPENDS = ""
SRC_URI = "\
	http://pkg.brokenlcd.net/distfiles/sources/libevent-2.0.21-stable.tar.gz \
           "
# For tarball packages (as opposed to git / svn which include the commit in the URI)
SRC_URI[md5sum] = "b2405cc9ebf264aa47ff615d9de527a2"
SRC_URI[sha256sum] = "22a530a8a5ba1cb9c080cba033206b17dacd21437762155c6d30ee6469f574f5"
PV = "2.0.21"
S = "${WORKDIR}/libevent-2.0.21-stable"
LIC_FILES_CHKSUM = "file://${S}/LICENSE;md5=45c5316ff684bcfe2f9f86d8b1279559"
do_configure () {
        ./configure --prefix=${prefix} --host=${TARGET_ARCH}
}
do_compile () {
        make
}
do_install () {
        DESTDIR=${D} oe_runmake install
}
