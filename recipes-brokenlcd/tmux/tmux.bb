DESCRIPTION = "Terminal multiplexer."
HOMEPAGE = "http://tmux.sourceforge.net"
LICENSE = "BSD"
DEPENDS = "libevent ncurses"
SRC_URI = "\
	http://pkg.brokenlcd.net/distfiles/sources/tmux-1.7.tar.gz \
           "
# For tarball packages (as opposed to git / svn which include the commit in the URI)
SRC_URI[md5sum] = "2c48fb9beb22eedba7a5de3b78dd0c03"
SRC_URI[sha256sum] = "68346bda11cf7d86591e663b94b98576332ac88c2890df26acb080f4440f9e7b"
PV = "1.7"
S = "${WORKDIR}/tmux-1.7"
LIC_FILES_CHKSUM = "file://${S}/NOTES;md5=78cc02844d8662c224e979faff6d8501"
do_configure () {
        ./configure --prefix=${prefix} --host=${TARGET_ARCH}
}
do_compile () {
        make
}
do_install () {
        DESTDIR=${D} oe_runmake install
}
