
DESCRIPTION = "A simple replacement for make(1)."
HOMEPAGE = "http://swtch.com/plan9port/unix/"
LICENSE = "lucent-public"
DEPENDS = ""
SRC_URI = "\
        http://swtch.com/plan9port/unix/mk-with-libs.tgz \
        file://mk.patch \
	file://mk-man.patch \
"
# For tarball packages (as opposed to git / svn which include the commit in the URI)
SRC_URI[md5sum] = "b4f5960b875258a08505e07f3404d639"
SRC_URI[sha256sum] = "0874882b54b00e6960eff4eed8c505cf8b711677aab59b079bbbf928f9001595"
PV = "2.0"
S = "${WORKDIR}/mk"
LIC_FILES_CHKSUM = "file://${S}/mk/NOTICE;md5=5fe0eff0b3f20304c5f44be0e163e0bb"
do_configure () {
	echo "consider me configured, bitches"
	env
}
do_compile () {
        make CC="${CC}" OBJTYPE=${TARGET_ARCH}
}
do_install () {
        DESTDIR=${D} oe_runmake PREFIX=${D}${prefix} install 
}
